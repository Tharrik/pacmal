// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GhostPawn.h"
#include "BlinkyPawn.generated.h"

/**
 * 
 */
UCLASS()
class PACMAL_API ABlinkyPawn : public AGhostPawn
{
	GENERATED_BODY()

public:
	virtual void FindTarget() override;

};
