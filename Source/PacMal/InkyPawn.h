// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GhostPawn.h"
#include "InkyPawn.generated.h"

/**
 * 
 */
UCLASS()
class PACMAL_API AInkyPawn : public AGhostPawn
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Ghost")
		class ABlinkyPawn * blinky;
	UPROPERTY(EditAnywhere, Category = "Ghost")
		int32 ambushDistance = 4;
	
public:
	virtual void FindTarget() override;

};
