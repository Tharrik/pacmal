// Fill out your copyright notice in the Description page of Project Settings.

#include "TileStruct.h"

const TCHAR * TileTypeToString(TileType type)
{
	switch (type) {
	case TileType::Navegable: return TEXT("Navegable");
	case TileType::Pen:	      return TEXT("Pen");
	case TileType::Solid:     return TEXT("Solid");
	}
	return TEXT("");
}

const TCHAR * TileContentToString(TileContent content)
{
	switch (content) {
	case TileContent::Dot:   return TEXT("Dot");
	case TileContent::Empty: return TEXT("Empty");
	case TileContent::Power: return TEXT("Power");
	}
	return TEXT("");
}

FTile::FTile()
{
	up = left = down = right = false;
}

FTile::FTile(TileType type, FIntPoint position)
	: type{ type }, position{ position }
{
	up = left = down = right = false;
}

const uint8 FTile::GetDirections() const
{
	uint8 directions = 0;

	if (up) ++directions;
	if (down) ++directions;
	if (left) ++directions;
	if (right) ++directions;

	return directions;
}

FString FTile::ToString() const
{
	FString string = FString::Printf(TEXT("{ \"x\": %i, \"y\": %i, \"type\": \"%s\", \"content\": \"%s\", \"Neighbors\": ["),
		GetRow(),
		GetColumn(),
		TileTypeToString(type),
		TileContentToString(content)
	);

	if (up) string.Append(TEXT(" \"up\","));
	if (down) string.Append(TEXT(" \"down\","));
	if (left) string.Append(TEXT(" \"left\","));
	if (right) string.Append(TEXT(" \"right\","));

	string.RemoveFromEnd(",");

	string.Append(TEXT(" ] }"));

	return MoveTemp(string);
}

