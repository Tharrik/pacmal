// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Chara.h"
#include "Board.h"
#include "GhostPawn.generated.h"

/**
 *
 */

UENUM()
enum class GhostState : int8 { Chase, Scatter, Frightened, Dead };

/*	TARGET BY STATE:
	Chase: According to ghost personality
	Scatter: Ghosts base tile
	Frightened: No target, random direction selection
	Dead: Pen tile at random.
*/

UCLASS()
class PACMAL_API AGhostPawn : public AChara
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, Category = "Ghost")
	GhostState state = GhostState::Scatter;

	UPROPERTY(EditAnywhere, Category = "Ghost")
	class APacPawn * pacman;

	UPROPERTY(VisibleAnywhere, Category = "Ghost")
	FIntPoint baseTilePosition;

	// Used to know when to execute direction change and chose the next one.
	UPROPERTY(VisibleAnywhere, Category = "Ghost")
	FIntPoint currentTilePosition;

	UPROPERTY(VisibleAnywhere, Category = "Ghost")
	FIntPoint target;

	UPROPERTY(VisibleAnywhere, Category = "Ghost")
	MoveDirection nextTurn;
	UPROPERTY(VisibleAnywhere, Category = "Ghost")
	MoveDirection currentTurn;

	UPROPERTY(VisibleAnywhere, Category = "Ghost")
	bool bNeedChangeDirection = false;

protected:
	void CheckTileChange();

public:
	virtual void SetBasePosition(FIntPoint position) { baseTilePosition = position; }

	virtual void UpdateLocation(float DeltaTime) override;
	virtual void UpdateTurns();

	virtual void TargetedTurning();
	virtual void AimlessTurning();

	virtual void Initialize() override;

	virtual void ChangeState(GhostState state);
	virtual void InvertDirection();

protected:
	virtual void FindTarget() {}
	virtual const FTile & NextTile();

};
