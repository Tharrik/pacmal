// Fill out your copyright notice in the Description page of Project Settings.

#include "GameBoard.h"
#include <Engine/World.h>
#include "PacPawn.h"
#include "BlinkyPawn.h"
#include "ClydePawn.h"
#include "PinkyPawn.h"
#include "InkyPawn.h"
#include "Board.h"
#include <Engine/Engine.h>

// Sets default values
AGameBoard::AGameBoard()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGameBoard::BeginPlay()
{
	Super::BeginPlay();
}

void AGameBoard::GenerateGeometry()
{
	UE_LOG(LogTemp, Warning, TEXT("GENERATING GEOMETRY"));

	const int rows = board.GetRows();
	const int columns = board.GetColumns();

	/*visualTiles.Empty(rows * columns);
	visualTiles.Reserve(rows * columns);*/

	for (int row = 0; row < rows; ++row) {
		for (int column = 0; column < columns; ++column) {

			FTile & tile = board.GetTileAt(row, column);

			//UE_LOG(LogTemp, Warning, TEXT("New tile at [%i][%i]: %s"), row, column, *tile.ToString());

			FVector newLocation = this->GetActorLocation() + FVector{ (row * tileSize), (column * tileSize), 0};

			switch(tile.GetType())
			{
				case TileType::Solid:
				{
					visualTiles.Add({row, column}, GetWorld()->SpawnActor<AActor>(solidTileClass, newLocation, FRotator::ZeroRotator));
					break;
				}
				case TileType::Navegable:
					switch (tile.GetContent())
					{
						case TileContent::Dot:
						{
							visualTiles.Add({ row, column }, GetWorld()->SpawnActor<AActor>(dotTileClass, newLocation, FRotator::ZeroRotator));
							break;
						}
						case TileContent::Power:
						{
							visualTiles.Add({ row, column }, GetWorld()->SpawnActor<AActor>(powerTileClass, newLocation, FRotator::ZeroRotator));
							break;
						}
					}
					break;

			}
			if (visualTiles.Contains(FIntPoint{ row, column })) {

				AActor * actor = visualTiles[FIntPoint{ row, column }];

				float scaleFactor = tileSize / 100.f;

				actor->SetActorScale3D(FVector::OneVector * scaleFactor);

				//if(actor) UE_LOG(LogTemp, Warning, TEXT("Tile created: %s"), *actor->GetName());
			}
		}
	}
}

void AGameBoard::RescalePawns()
{
	float scaleFactor = tileSize / 100.f;

	if(pacmanPawn) {
		pacmanPawn->Reescale(scaleFactor);
	}

	if (blinkyPawn) {
		blinkyPawn->Reescale(scaleFactor);
	}

	if (inkyPawn) {
		inkyPawn->Reescale(scaleFactor);
	}

	if (pinkyPawn) {
		pinkyPawn->Reescale(scaleFactor);
	}

	if (clydePawn) {
		clydePawn->Reescale(scaleFactor);
	}
}

void AGameBoard::RelocatePawns()
{
	if (pacmanPawn)
	{
		float x = board.PacManTile().GetRow() * tileSize;
		float y = board.PacManTile().GetColumn() * tileSize;
		pacmanPawn->SetActorLocation(GetActorLocation() + FVector{x, y, 0});
	}

	if (blinkyPawn) {
		float x = board.BlinkyTile().GetRow() * tileSize;
		float y = board.BlinkyTile().GetColumn() * tileSize;
		blinkyPawn->SetActorLocation(GetActorLocation() + FVector{ x, y, 0 });
	}

	if (inkyPawn) {
		float x = board.InkyTile().GetRow() * tileSize;
		float y = board.InkyTile().GetColumn() * tileSize;
		inkyPawn->SetActorLocation(GetActorLocation() + FVector{ x, y, 0 });
	}

	if (pinkyPawn) {
		float x = board.PinkyTile().GetRow() * tileSize;
		float y = board.PinkyTile().GetColumn() * tileSize;
		pinkyPawn->SetActorLocation(GetActorLocation() + FVector{ x, y, 0 });
	}

	if (clydePawn) {
		float x = board.ClydeTile().GetRow() * tileSize;
		float y = board.ClydeTile().GetColumn() * tileSize;
		clydePawn->SetActorLocation(GetActorLocation() + FVector{ x, y, 0 });
	}
}

void AGameBoard::UpdateGhostState(float Deltatime)
{
	if (ghostCycleIndex >= ghostCycle.Num()) return;

	timeSinceStateChange += Deltatime;

	/*UE_LOG(LogTemp, Warning, TEXT("Time for ghost state change: %f"),
		ghostCycle[ghostCycleIndex] - timeSinceStateChange);*/

	if (timeSinceStateChange >= ghostCycle[ghostCycleIndex]) {

		timeSinceStateChange -= ghostCycle[ghostCycleIndex];

		++ghostCycleIndex;

		GhostState newState;

		if (ghostCycleIndex % 2 == 0 && ghostCycleIndex < ghostCycle.Num()) {
			newState = GhostState::Scatter;
		}
		else {
			newState = GhostState::Chase;
		}

		blinkyPawn->ChangeState(newState);
		inkyPawn->ChangeState(newState);
		pinkyPawn->ChangeState(newState);
		clydePawn->ChangeState(newState);
	}
	
}

void AGameBoard::UpdateDotTiles()
{
	FTile & pacmanTile = board.GetTileAt(GetPositionFromLocation(pacmanPawn->GetActorLocation()));
	switch (pacmanTile.GetContent())
	{
	case TileContent::Dot:
	case TileContent::Power:
		FIntPoint pacmanPosition = GetPositionFromLocation(pacmanPawn->GetActorLocation());
		AActor * tile = visualTiles[pacmanPosition];
		// UE_LOG(LogTemp, Warning, TEXT("Hidding %s"), *tile->GetName());
		tile->SetActorHiddenInGame(true);
		pacmanTile.SetContent(TileContent::Empty);
		break;
	}
}

void AGameBoard::CheckPacmanDeath()
{
	FIntPoint pacmanTile = GetPositionFromLocation(pacmanPawn->GetActorLocation());
	FIntPoint blinkyTile = GetPositionFromLocation(blinkyPawn->GetActorLocation());
	FIntPoint   inkyTile = GetPositionFromLocation(  inkyPawn->GetActorLocation());
	FIntPoint  pinkyTile = GetPositionFromLocation( pinkyPawn->GetActorLocation());
	FIntPoint  clydeTile = GetPositionFromLocation( clydePawn->GetActorLocation());
	if (pacmanTile == blinkyTile
		|| pacmanTile == inkyTile
		|| pacmanTile == pinkyTile
		|| pacmanTile == clydeTile) {
		GEngine->DeferredCommands.Add(TEXT("pause"));
	}
}

FIntPoint AGameBoard::GetPositionFromLocation(FVector location)
{
	// To local
	location -= GetActorLocation();

	int row = FMath::RoundToInt(location.X / tileSize);
	int column = FMath::RoundToInt(location.Y / tileSize);

	return FIntPoint{ row, column };
}

// Called every frame
void AGameBoard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Update characters' locations
	if (pacmanPawn && !pacmanPawn->bHidden) {
		pacmanPawn->UpdateLocation(DeltaTime);
	}
	
	if (blinkyPawn) {
		blinkyPawn->UpdateLocation(DeltaTime);
	}

	if (inkyPawn) {
		inkyPawn->UpdateLocation(DeltaTime);
	}

	if (pinkyPawn) {
		pinkyPawn->UpdateLocation(DeltaTime);
	}

	if (clydePawn) {
		clydePawn->UpdateLocation(DeltaTime);
	}

	UpdateGhostState(DeltaTime);
	UpdateDotTiles();
	CheckPacmanDeath();
}

void AGameBoard::Initialize()
{
	board = { *filename };
	GenerateGeometry();
	RescalePawns();
	RelocatePawns();
	InitializePawns();
}

FVector AGameBoard::GetCameraLocation(float fov)
{
	float x = GetHeight() * .5f;
	float y = GetWidth() * .5f;
	float z = y / FMath::Tan(FMath::DegreesToRadians(fov * .5f));

	FVector cameraLocation = this->GetActorLocation() + FVector{ x, y, z };
	return cameraLocation;
}

FTile & AGameBoard::GetTileFromLocation(const FVector & tileLocation)
{
	FIntPoint tilePosition = GetPositionFromLocation(tileLocation);

	if (tilePosition.X >= 0
		&& tilePosition.X < board.GetRows()
		&& tilePosition.Y >= 0
		&& tilePosition.Y < board.GetColumns()) {
		return board.GetTileAt(tilePosition);
	}

	UE_LOG(LogTemp, Warning, TEXT("%s: No tile found at %s!"), *GetName(), *tileLocation.ToString());
	GEngine->DeferredCommands.Add(TEXT("pause"));
	checkf(false, TEXT("No tile at given location!"));
	return *new FTile();
}

FTile & AGameBoard::GetTileAt(const FIntPoint position)
{
	return board.GetTileAt(position);
}

FTile & AGameBoard::GetTileUp(const FIntPoint position)
{
	return board.GetTileUp(position);
}

FTile & AGameBoard::GetTileLeft(const FIntPoint position)
{
	return board.GetTileLeft(position);
}

FTile & AGameBoard::GetTileDown(const FIntPoint position)
{
	return board.GetTileDown(position);
}

FTile & AGameBoard::GetTileRight(const FIntPoint position)
{
	return board.GetTileRight(position);
}

FVector AGameBoard::GetTileLocation(FIntPoint position)
{
	return this->GetActorLocation() + FVector{ (position.X * tileSize), (position.Y * tileSize), 0 };
}

void AGameBoard::InitializePawns()
{
	if (pacmanPawn) pacmanPawn->SetBoard(this);

	if (blinkyPawn) {
		blinkyPawn->SetBoard(this);
		blinkyPawn->SetBasePosition(board.BlinkyTile().GetPosition());
		blinkyPawn->Initialize();
	}

	if (inkyPawn) {
		inkyPawn->SetBoard(this);
		inkyPawn->SetBasePosition(board.InkyTile().GetPosition());
		inkyPawn->Initialize();
	}

	if (pinkyPawn) {
		pinkyPawn->SetBoard(this);
		pinkyPawn->SetBasePosition(board.PinkyTile().GetPosition());
		pinkyPawn->Initialize();
	}

	if (clydePawn) {
		clydePawn->SetBoard(this);
		clydePawn->SetBasePosition(board.ClydeTile().GetPosition());
		clydePawn->Initialize();
	}

}

