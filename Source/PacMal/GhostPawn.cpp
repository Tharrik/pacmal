// Fill out your copyright notice in the Description page of Project Settings.

#include "GhostPawn.h"
#include "GameBoard.h"
#include <basetsd.h>
#include <Engine/Engine.h>

void AGhostPawn::CheckTileChange()
{
	FIntPoint newCurrentTile = gameBoard->GetPositionFromLocation(GetActorLocation());
	if (newCurrentTile != currentTilePosition) {
		currentTilePosition = newCurrentTile;
		UpdateTurns();
		bNeedChangeDirection = true;
	}
}

void AGhostPawn::UpdateLocation(float DeltaTime)
{
	Super::UpdateLocation(DeltaTime);

	if(!bNeedChangeDirection) {
		CheckTileChange();
	}
	else {

		FVector currentLocation = GetActorLocation();
		FVector tileLocation = gameBoard->GetTileLocation(currentTilePosition);

		// TRY TO CHANGE DIRECTION
		switch (direction) {

		case MoveDirection::Up: {
			// MOVING UP
			// Check if it has passed the center of the current tile.
			if (currentLocation.X > tileLocation.X) {
				// Calculate distance from center
				float distanceFromCenter =  currentLocation.X - tileLocation.X;
				bNeedChangeDirection = false;
				// Change direction
				switch (currentTurn) {
					case MoveDirection::Down:
						SetActorLocation(tileLocation + FVector{ -distanceFromCenter, 0.f, 0.f });
						GoDown();
						break;
					case MoveDirection::Left:
						SetActorLocation(tileLocation + FVector{ 0.f, -distanceFromCenter, 0.f });
						GoLeft();
						break;
					case MoveDirection::Right:
						SetActorLocation(tileLocation + FVector{ 0.f, distanceFromCenter, 0.f });
						GoRight();
						break;
				}
			}
			break;
		}

		case MoveDirection::Down:
			// MOVING DOWN
			// Check if it has passed the center of the current tile.
			if (currentLocation.X < tileLocation.X) {
				// Calculate distance from center
				float distanceFromCenter =  tileLocation.X - currentLocation.X;
				bNeedChangeDirection = false;
				// Change direction
				switch (currentTurn) {
				case MoveDirection::Up:
					SetActorLocation(tileLocation + FVector{ distanceFromCenter, 0.f, 0.f });
					GoUp();
					break;
				case MoveDirection::Left:
					SetActorLocation(tileLocation + FVector{ 0.f, -distanceFromCenter, 0.f });
					GoLeft();
					break;
				case MoveDirection::Right:
					SetActorLocation(tileLocation + FVector{ 0.f, distanceFromCenter, 0.f });
					GoRight();
					break;
				}
			}
			break;

		case MoveDirection::Left:
			// MOVING LEFT
			// Check if it has passed the center of the current tile.
			if (currentLocation.Y < tileLocation.Y) {
				// Calculate distance from center
				float distanceFromCenter = tileLocation.Y - currentLocation.Y;
				bNeedChangeDirection = false;
				// Change direction
				switch (currentTurn) {
				case MoveDirection::Up:
					SetActorLocation(tileLocation + FVector{ distanceFromCenter, 0.f, 0.f });
					GoUp();
					break;
				case MoveDirection::Down:
					SetActorLocation(tileLocation + FVector{ -distanceFromCenter, 0.f, 0.f });
					GoDown();
					break;
				case MoveDirection::Right:
					SetActorLocation(tileLocation + FVector{ 0.f, distanceFromCenter, 0.f });
					GoRight();
					break;
				}
			}
			break;

		case MoveDirection::Right:
			// MOVING RIGHT
			// Check if it has passed the center of the current tile.
			if (currentLocation.Y > tileLocation.Y) {
				// Calculate distance from center
				float distanceFromCenter = currentLocation.Y - tileLocation.Y;
				bNeedChangeDirection = false;
				// Change direction
				switch (currentTurn) {
				case MoveDirection::Up:
					SetActorLocation(tileLocation + FVector{ distanceFromCenter, 0.f, 0.f });
					GoUp();
					break;
				case MoveDirection::Down:
					SetActorLocation(tileLocation + FVector{ -distanceFromCenter, 0.f, 0.f });
					GoDown();
					break;
				case MoveDirection::Left:
					SetActorLocation(tileLocation + FVector{ 0.f, -distanceFromCenter, 0.f });
					GoLeft();
					break;
				}
			}
			break;
		}

	}
}

void AGhostPawn::UpdateTurns()
{
	currentTurn = nextTurn;

	switch (state)
	{
	case GhostState::Chase:
		TargetedTurning();
		break;
	case GhostState::Scatter:
		TargetedTurning();
		break;
	case GhostState::Frightened:
		AimlessTurning();
		break;
	case GhostState::Dead:
		// TODO Implement Dead behavior
		break;
	}

}

void AGhostPawn::TargetedTurning()
{
	FindTarget();

	const FTile & nextTile = NextTile();

	if (nextTile.GetDirections() == 2) {
		if (nextTile.Up() && currentTurn != MoveDirection::Down) nextTurn = MoveDirection::Up;
		else if (nextTile.Down() && currentTurn != MoveDirection::Up) nextTurn = MoveDirection::Down;
		else if (nextTile.Left() && currentTurn != MoveDirection::Right) nextTurn = MoveDirection::Left;
		else if (nextTile.Right() && currentTurn != MoveDirection::Left) nextTurn = MoveDirection::Right;
	}
	else {
		int32 currentDistance = MAXINT32;

		// UE_LOG(LogTemp, Warning, TEXT("%s decides direction for %s"), *GetName(), *nextTile.ToString());

		if (nextTile.Up() && direction != MoveDirection::Down) {
			FTile & tileUp = gameBoard->GetTileUp(nextTile.GetPosition());
			FIntPoint distanceVector = tileUp.GetPosition() - target;
			int32 newDistance = distanceVector.X * distanceVector.X + distanceVector.Y * distanceVector.Y;
			// UE_LOG(LogTemp, Warning, TEXT("Up distance: %i"), newDistance);
			if (newDistance < currentDistance) {
				currentDistance = newDistance;
				nextTurn = MoveDirection::Up;
			}
		}

		if (nextTile.Left() && direction != MoveDirection::Right) {
			FTile & tileLeft= gameBoard->GetTileLeft(nextTile.GetPosition());
			FIntPoint distanceVector = tileLeft.GetPosition() - target;
			int32 newDistance = distanceVector.X * distanceVector.X + distanceVector.Y * distanceVector.Y;
			// UE_LOG(LogTemp, Warning, TEXT("Left distance: %i"), newDistance);
			if (newDistance < currentDistance) {
				currentDistance = newDistance;
				nextTurn = MoveDirection::Left;
			}
		}

		if (nextTile.Down() && direction != MoveDirection::Up) {
			FTile & tileDown = gameBoard->GetTileDown(nextTile.GetPosition());
			FIntPoint distanceVector = tileDown.GetPosition() - target;
			int32 newDistance = distanceVector.X * distanceVector.X + distanceVector.Y * distanceVector.Y;
			// UE_LOG(LogTemp, Warning, TEXT("Down distance: %i"), newDistance);
			if (newDistance < currentDistance) {
				currentDistance = newDistance;
				nextTurn = MoveDirection::Down;
			}
		}

		if (nextTile.Right() && direction != MoveDirection::Left) {
			FTile & tileRight = gameBoard->GetTileRight(nextTile.GetPosition());
			FIntPoint distanceVector = tileRight.GetPosition() - target;
			int32 newDistance = distanceVector.X * distanceVector.X + distanceVector.Y * distanceVector.Y;
			// UE_LOG(LogTemp, Warning, TEXT("Right distance: %i"), newDistance);
			if (newDistance < currentDistance) {
				currentDistance = newDistance;
				nextTurn = MoveDirection::Right;
			}
		}
	}
}

void AGhostPawn::AimlessTurning()
{
	const FTile & nextTile = NextTile();

	bool repeat = true;

	do {
		int randomNumber = 1 + FMath::Rand() % 4;
		switch (randomNumber) {
		case 1:
			if (nextTile.Up() && currentTurn != MoveDirection::Down) {
				nextTurn = MoveDirection::Up;
				repeat = false;
			}
			break;
		case 2:
			if (nextTile.Down() && currentTurn != MoveDirection::Up) {
				nextTurn = MoveDirection::Down;
				repeat = false;
			}
			break;
		case 3:
			if (nextTile.Left() && currentTurn != MoveDirection::Right) {
				nextTurn = MoveDirection::Left;
				repeat = false;
			}
			break;
		case 4:
			if (nextTile.Right() && currentTurn != MoveDirection::Left) {
				nextTurn = MoveDirection::Right;
				repeat = false;
			}
			break;
		}
	} while (repeat);
}

void AGhostPawn::Initialize()
{
	Super::Initialize();
	nextTurn = currentTurn = direction;
}

void AGhostPawn::ChangeState(GhostState newState)
{
	// TODO Repair Invert direction. It can cause errors in the turns.
	// InvertDirection();

	state = newState;
}

void AGhostPawn::InvertDirection()
{
	// TODO We need to consider changing current turn and next turn
	if (bNeedChangeDirection) {
		// Hasn't reached the center of the tile yet. We need to change the current turn and calculate the next turn.
		// THIS DOES NOT WORK: nextTurn = currentTurn = MoveDirection::...;
		switch (direction)
		{
		case MoveDirection::Up:
			nextTurn = currentTurn = MoveDirection::Down;
			break;
		case MoveDirection::Down:
			nextTurn = currentTurn = MoveDirection::Up;
			break;
		case MoveDirection::Left:
			nextTurn = currentTurn = MoveDirection::Right;
			break;
		case MoveDirection::Right:
			nextTurn = currentTurn = MoveDirection::Left;
			break;
		}

	}
	else {
		// Is exiting the tile. We only need to update the next turn, as it will overwrite the current turn
		// as soon as it reaches the next tile.
		switch (direction)
		{
		case MoveDirection::Up:
			nextTurn = MoveDirection::Down;
			break;
		case MoveDirection::Down:
			nextTurn = MoveDirection::Up;
			break;
		case MoveDirection::Left:
			nextTurn = MoveDirection::Right;
			break;
		case MoveDirection::Right:
			nextTurn = MoveDirection::Left;
			break;
		}
	}
}

const FTile & AGhostPawn::NextTile()
{
	// Get next tile
	switch (currentTurn) {
	case MoveDirection::Up:
		return gameBoard->GetTileUp(currentTilePosition);
	case MoveDirection::Down:
		return gameBoard->GetTileDown(currentTilePosition);
	case MoveDirection::Left:
		return gameBoard->GetTileLeft(currentTilePosition);
	case MoveDirection::Right:
		return gameBoard->GetTileRight(currentTilePosition);
	}

	UE_LOG(LogTemp, Warning, TEXT("%s: No next file found!"), *GetName());
	GEngine->DeferredCommands.Add(TEXT("pause"));
	checkf(false, TEXT("There is no next tile available!"));
	return *new FTile();
}
