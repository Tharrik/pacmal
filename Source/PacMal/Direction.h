// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <NoExportTypes.h>

/**
 *
 */
class Direction
{

public:

	static const Direction UP;
	static const Direction DOWN;
	static const Direction LEFT;
	static const Direction RIGHT;
	static const Direction ZERO;

	virtual ~Direction();

protected:
	FVector vector;

protected:
	Direction(float x, float y) : vector{ FVector{x, y, 0} } {}

public:

	operator const FVector & () { return vector; }
	operator FVector & () { return vector; }

};

Direction const Direction::UP = { 1.f, 0.f };
Direction const Direction::DOWN = { -1.f, 0.f };
Direction const Direction::LEFT = { 0.f, -1.f };
Direction const Direction::RIGHT = { 0.f, 1.f };
Direction const Direction::ZERO = { 0.f, 0.f };
