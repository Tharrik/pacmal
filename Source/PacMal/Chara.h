// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Chara.generated.h"

UENUM()
enum class MoveDirection : int8 { None, Up, Down, Left, Right };

UCLASS()
class PACMAL_API AChara : public APawn
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, Category = "Character")
	class UStaticMeshComponent * meshComponent;

	UPROPERTY(EditAnywhere, Category = "Character")
	float maxSpeed;

	UPROPERTY(VisibleAnywhere, Category = "Character")
	FVector directionVector = FVector::ZeroVector;

	UPROPERTY(EditAnywhere, Category = "Character")
	MoveDirection direction = MoveDirection::None;

	class AGameBoard * gameBoard;

public:
	// Sets default values for this pawn's properties
	AChara();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void UpdateLocation(float DeltaTime);

	virtual MoveDirection GetDirection() const { return direction; }

	virtual void SetBoard(AGameBoard * board);
	virtual void Reescale(float scaleVector);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// These functions modify the direction if is available in the current tile.
	// Derived classes must override to improve movement.
	virtual void GoUp();
	virtual void GoDown();
	virtual void GoLeft();
	virtual void GoRight();
	virtual void Stop();

	virtual void Initialize();
};
