// Fill out your copyright notice in the Description page of Project Settings.

#include "Board.h"
#include <Core/Public/Misc/FileHelper.h>
#include <Runtime/Core/Public/HAL/PlatformFilemanager.h>
#include <Runtime/Core/Public/Misc/Paths.h>
#include <Runtime/Core/Public/Math/UnrealMathUtility.h>




FBoard::FBoard(const TCHAR * filename)
{
	UE_LOG(LogTemp, Warning, TEXT("LOADING BOARD FILE"));

	IPlatformFile& platformFile = FPlatformFileManager::Get().GetPlatformFile();
	FString absoluteFilePath = FPaths::ProjectContentDir().Append(filename);
	if (!platformFile.FileExists(*absoluteFilePath))
	{
		absoluteFilePath = FPaths::GameContentDir().Append(filename);
	}

	if (platformFile.FileExists(*absoluteFilePath)) {
		FString fileString;
		if (FFileHelper::LoadFileToString(fileString, *absoluteFilePath)) {
			UE_LOG(LogTemp, Warning, TEXT("File content: %s"), *fileString);
			ParseFile(fileString);
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("Coudn't load from file %s!"), *absoluteFilePath);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("%s does not exist!"), *absoluteFilePath);
	}
	// TestPrint();
}

FTile & FBoard::GetTileAt(int32 row, int32 column)
{
	return tiles[row * columns + column];
}

FTile & FBoard::GetTileUp(const FIntPoint position)
{
	int32 rowUp = (position.X + 1) % rows;
	return GetTileAt(rowUp, position.Y);
}

FTile & FBoard::GetTileLeft(const FIntPoint position)
{
	int32 columnLeft = (position.Y - 1 + columns) % columns;
	return GetTileAt(position.X, columnLeft);
}

FTile & FBoard::GetTileDown(const FIntPoint position)
{
	int32 rowDown = (position.X - 1 + rows) % rows;
	return GetTileAt(rowDown, position.Y);
}

FTile & FBoard::GetTileRight(const FIntPoint position)
{
	int32 columnRight = (position.Y + 1) % columns;
	return GetTileAt(position.X, columnRight);
}

void FBoard::TestPrint()
{
	FString result{};
	for (int32 i = 0; i < rows * columns; ++i) {
		result.Append(tiles[i].ToString() + "\n");
	}
	UE_LOG(LogTemp, Warning, TEXT("%s"), *result);
}

void FBoard::ParseFile(FString fileContent)
{
	// Convert to array
	TArray<FString> lines;
	fileContent.ParseIntoArrayLines(lines);

	// Get size
	rows = lines.Num();
	columns = lines[0].Len();
	UE_LOG(LogTemp, Warning, TEXT("Map Size: %ix%i"), rows, columns);

	// Create bidimensional array
	// tiles = static_cast<Tile*>(operator new[](rows * columns * sizeof(Tile)));
	tiles.Empty(rows * columns);
	tiles.Reserve(rows * columns);

	// Create tiles
	for (int32 row = 0; row < rows; ++row) {
		for (int32 column = 0; column < columns; ++column) {
			switch (lines[row][column]) {

			case 'S':
				// Solid tile
				tiles.Add(FTile(TileType::Solid, { row, column }));
				break;

			case ' ':
				// Empty navigable tile
				tiles.Add(FTile(TileType::Navegable, { row, column }));
				break;

			case 'o':
				// Navigable tile with dot
				tiles.Add(FTile(TileType::Navegable, { row, column }));
				tiles[row * columns + column].SetContent(TileContent::Dot);
				break;

			case 'O':
				// Navigable tile with power up
				tiles.Add(FTile(TileType::Navegable, { row, column }));
				tiles[row * columns + column].SetContent(TileContent::Power);
				break;

			case 'G':
				// Ghost Pen
				tiles.Add(FTile(TileType::Pen, { row, column }));
				break;

			case 'C':
				// Clyde base tile
				tiles.Add(FTile(TileType::Navegable, { row, column }));
				tiles[row * columns + column].SetContent(TileContent::Dot);
				clydeBase = FIntPoint{row, column};
				break;

			case 'I':
				// Inky base tile
				tiles.Add(FTile(TileType::Navegable, { row, column }));
				tiles[row * columns + column].SetContent(TileContent::Dot);
				inkyBase = FIntPoint{ row, column };
				break;

			case 'R':
				// Pinky base tile
				tiles.Add(FTile(TileType::Navegable, { row, column }));
				tiles[row * columns + column].SetContent(TileContent::Dot);
				pinkyBase = FIntPoint{ row, column };
				break;

			case 'B':
				// Blinky base tile
				tiles.Add(FTile(TileType::Navegable, { row, column }));
				tiles[row * columns + column].SetContent(TileContent::Dot);
				blinkyBase = FIntPoint{ row, column };
				break;

			case 'P':
				// PacMan starting point
				tiles.Add(FTile(TileType::Navegable, { row, column }));
				pacmanBase = FIntPoint{ row, column };
				break;
			}
		}
	}

	// Assign neighbors
	for (int32 row = 0; row < rows; ++row) {
		for (int32 column = 0; column < columns; ++column) {
			FTile & currentTile = GetTileAt(row, column);

			if (row == 29 && column == 7) {
				int i = 0;
				++i;
				currentTile = GetTileAt(row, column);
			}

			switch (currentTile.GetType()) {
			case TileType::Navegable: {

				// Up
				if (GetTileUp({ row, column }).GetType() == TileType::Navegable){
					GetTileAt(row, column).SetUp(true);
				}

				// Left
				if (GetTileLeft({ row, column }).GetType() == TileType::Navegable) {
					GetTileAt(row, column).SetLeft(true);
				}

				// Down
				if (GetTileDown({ row, column }).GetType() == TileType::Navegable) {
					GetTileAt(row, column).SetDown(true);
				}

				// Right
				if (GetTileRight({ row, column }).GetType() == TileType::Navegable) {
					GetTileAt(row, column).SetRight(true);
				}

				break;
			}
			case TileType::Pen:

				// Up
				if (GetTileUp({ row, column }).GetType() == TileType::Navegable
					|| GetTileUp({ row, column }).GetType() == TileType::Pen) {
					GetTileAt(row, column).SetUp(true);
				}

				// Left
				if (GetTileLeft({ row, column }).GetType() == TileType::Navegable
					|| GetTileLeft({ row, column }).GetType() == TileType::Pen) {
					GetTileAt(row, column).SetLeft(true);
				}

				// Down
				if (GetTileDown({ row, column }).GetType() == TileType::Navegable
					|| GetTileDown({ row, column }).GetType() == TileType::Pen) {
					GetTileAt(row, column).SetDown(true);
				}

				// Right
				if (GetTileRight({ row, column }).GetType() == TileType::Navegable
					|| GetTileRight({ row, column }).GetType() == TileType::Pen) {
					GetTileAt(row, column).SetRight(true);
				}

				break;
			}
		}
	}
}

