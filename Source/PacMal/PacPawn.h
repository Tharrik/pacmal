// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Chara.h"
#include <Components/MeshComponent.h>
#include "PacPawn.generated.h"

/**
 *
 */
UCLASS()
class PACMAL_API APacPawn : public AChara
{
	GENERATED_BODY()

public:

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void UpdateLocation(float DeltaTime) override;

	// Checks if the current direction is valid.
	virtual void CheckDirection();

	virtual void GoUp() override;
	virtual void GoDown() override;
	virtual void GoLeft() override;
	virtual void GoRight() override;

};
