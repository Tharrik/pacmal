// Fill out your copyright notice in the Description page of Project Settings.

#include "ClydePawn.h"
#include "PacPawn.h"
#include "GameBoard.h"

void AClydePawn::FindTarget()
{
	// Check distance from pacman
	FVector distanceVector = GetActorLocation() - pacman->GetActorLocation();

	if (distanceVector.SizeSquared() > FMath::Square(gameBoard->GetTileSize() * fearFactor)) {
		// If Clyde is far from Pac Man, he will chase him.
		target = gameBoard->GetPositionFromLocation(pacman->GetActorLocation());
	}
	else {
		// If Pac Man is too close, Clyde will be scared and will run home.
		target = baseTilePosition;
	}

}
