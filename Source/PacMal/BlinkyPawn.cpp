// Fill out your copyright notice in the Description page of Project Settings.

#include "BlinkyPawn.h"
#include "PacPawn.h"
#include "GameBoard.h"

void ABlinkyPawn::FindTarget()
{
	switch (state) {
	case GhostState::Chase:
		target = gameBoard->GetTileFromLocation(pacman->GetActorLocation()).GetPosition();
		break;
	case GhostState::Scatter:
		target = baseTilePosition;
		break;
	}
}
