// Fill out your copyright notice in the Description page of Project Settings.

#include "PinkyPawn.h"
#include "PacPawn.h"
#include "GameBoard.h"

void APinkyPawn::FindTarget()
{
	target = gameBoard->GetPositionFromLocation(pacman->GetActorLocation());
	switch (pacman->GetDirection())
	{
	case MoveDirection::Up:
		target += FIntPoint{ ambushDistance, 0 };
		break;
	case MoveDirection::Down:
		target += FIntPoint{ -ambushDistance, 0 };
		break;
	case MoveDirection::Left:
		target += FIntPoint{ 0, -ambushDistance };
		break;
	case MoveDirection::Right:
		target += FIntPoint{ 0, ambushDistance };
		break;
	}
}
