// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TileStruct.generated.h"

UENUM()
enum class TileType : uint8 { Navegable, Solid, Pen };
const TCHAR * TileTypeToString(TileType type);

UENUM()
enum class TileContent : uint8 { Empty, Dot, Power };
const TCHAR * TileContentToString(TileContent content);

/**
 * 
 */
USTRUCT()
struct FTile
{
	GENERATED_BODY();

private:
	UPROPERTY(VisibleAnywhere)
		TileType type = TileType::Navegable;
	UPROPERTY(VisibleAnywhere)
		TileContent content = TileContent::Empty;

	UPROPERTY()
		uint8 up : 1;
	UPROPERTY()
		uint8 left : 1;
	UPROPERTY()
		uint8 down : 1;
	UPROPERTY()
		uint8 right : 1;

	/*UPROPERTY(VisibleAnywhere)
		bool up;
	UPROPERTY(VisibleAnywhere)
		bool left;
	UPROPERTY(VisibleAnywhere)
		bool down;
	UPROPERTY(VisibleAnywhere)
		bool right;*/

	UPROPERTY(VisibleAnywhere)
		FIntPoint position;

public:
	FTile();
	FTile(TileType type, FIntPoint position);

public:
	TileType GetType() const { return type; }
	TileContent GetContent() const { return content; }
	int32 GetRow() const { return position.X; }
	int32 GetColumn() const { return position.Y; }
	FIntPoint GetPosition() const { return position; }
	const uint8 GetDirections() const;

	void SetUp(bool value) { up = value; }
	void SetLeft(bool value) { left = value; }
	void SetDown(bool value) { down = value; }
	void SetRight(bool value) { right = value; }

	bool Up() const { return up; }
	bool Left() const { return left; }
	bool Down() const { return down; }
	bool Right() const { return right; }

	void SetContent(TileContent content) { this->content = content; }

	FString ToString() const;
};

