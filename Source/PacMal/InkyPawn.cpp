// Fill out your copyright notice in the Description page of Project Settings.

#include "InkyPawn.h"
#include "BlinkyPawn.h"
#include "PacPawn.h"
#include "GameBoard.h"

void AInkyPawn::FindTarget()
{
	FIntPoint inFrontOfPacman = gameBoard->GetPositionFromLocation(pacman->GetActorLocation());

	switch (pacman->GetDirection())
	{
	case MoveDirection::Up:
		inFrontOfPacman += FIntPoint{ ambushDistance, 0 };
		break;
	case MoveDirection::Down:
		inFrontOfPacman += FIntPoint{ -ambushDistance, 0 };
		break;
	case MoveDirection::Left:
		inFrontOfPacman += FIntPoint{ 0, -ambushDistance };
		break;
	case MoveDirection::Right:
		inFrontOfPacman += FIntPoint{ 0, ambushDistance };
		break;
	}

	FIntPoint blinkyPosition = gameBoard->GetPositionFromLocation(blinky->GetActorLocation());
	// Target = infront + vector from blinky to infront = infront + infront - blinky = infront * 2 - blinky
	target =  (inFrontOfPacman * 2) - blinkyPosition;
}
