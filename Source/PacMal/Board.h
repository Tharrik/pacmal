// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TileStruct.h"
#include "Board.generated.h"

/**
 *
 */
USTRUCT()
struct FBoard
{
	GENERATED_BODY()

private:
	UPROPERTY(VisibleAnywhere)
	TArray<FTile> tiles;

	UPROPERTY(VisibleAnywhere)
		int32 rows;
	UPROPERTY(VisibleAnywhere)
		int32 columns;

	UPROPERTY(VisibleAnywhere)
		FIntPoint pacmanBase;
	UPROPERTY(VisibleAnywhere)
		FIntPoint blinkyBase;
	UPROPERTY(VisibleAnywhere)
		FIntPoint pinkyBase;
	UPROPERTY(VisibleAnywhere)
		FIntPoint inkyBase;
	UPROPERTY(VisibleAnywhere)
		FIntPoint clydeBase;

public:

	FBoard() {}
	FBoard(const TCHAR * filename);
	~FBoard() {}

	inline int32 GetRows() { return rows; }
	inline int32 GetColumns() { return columns; }
	FTile & GetTileAt(int32 row, int32 column);
	FTile & GetTileAt(FIntPoint position) { return GetTileAt(position.X, position.Y); }

	FTile & GetTileUp(const FIntPoint position);
	FTile & GetTileLeft(const FIntPoint position);
	FTile & GetTileDown(const FIntPoint position);
	FTile & GetTileRight(const FIntPoint position);

	inline FTile & PacManTile() { return GetTileAt(pacmanBase); }
	inline FTile & BlinkyTile() { return GetTileAt(blinkyBase); }
	inline FTile & InkyTile() { return GetTileAt(inkyBase); }
	inline FTile & PinkyTile() { return GetTileAt(pinkyBase); }
	inline FTile & ClydeTile() { return GetTileAt(clydeBase); }

	void TestPrint();

private:
	void ParseFile(FString fileContent);

};
