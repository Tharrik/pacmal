// Fill out your copyright notice in the Description page of Project Settings.

#include "Chara.h"
#include "Board.h"
#include "GameBoard.h"

// Sets default values
AChara::AChara()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Body"));
	RootComponent = meshComponent;
	
}

// Called when the game starts or when spawned
void AChara::BeginPlay()
{
	Super::BeginPlay();
}

void AChara::GoUp()
{
	FTile currentTile = gameBoard->GetTileFromLocation(GetActorLocation());
	if(currentTile.Up())
	{
		direction = MoveDirection::Up;
		directionVector.Set(1.f, 0.f, 0.f);
	}
}

void AChara::GoDown()
{
	FTile currentTile = gameBoard->GetTileFromLocation(GetActorLocation());
	if (currentTile.Down())
	{
		direction = MoveDirection::Down;
		directionVector.Set(-1.f, 0.f, 0.f);
	}
}

void AChara::GoLeft()
{
	FTile currentTile = gameBoard->GetTileFromLocation(GetActorLocation());
	if (currentTile.Left())
	{
		direction = MoveDirection::Left;
		directionVector.Set(0.f, -1.f, 0.f);
	}
}

void AChara::GoRight()
{
	FTile currentTile = gameBoard->GetTileFromLocation(GetActorLocation());
	if (currentTile.Right())
	{
		direction = MoveDirection::Right;
		directionVector.Set(0.f, 1.f, 0.f);
	}
}

void AChara::Stop()
{
	direction = MoveDirection::None;
	directionVector.Set(0.f, 0.f, 0.f);
	UE_LOG(LogTemp, Warning, TEXT("%s stopped!"), *GetName());
}

void AChara::Initialize()
{
	// Initialize direction
	switch (direction) {
	case MoveDirection::Up:
		GoUp();
		break;
	case MoveDirection::Down:
		GoDown();
		break;
	case MoveDirection::Left:
		GoLeft();
		break;
	case MoveDirection::Right:
		GoRight();
		break;
	}
}

// Called every frame
void AChara::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AChara::UpdateLocation(float DeltaTime)
{
	// FBoard::Tile currentTile = board->GetTileFromLocation(GetActorLocation());
	SetActorLocation(GetActorLocation() + directionVector * DeltaTime * maxSpeed);
}

void AChara::SetBoard(AGameBoard * board)
{
	this->gameBoard = board;
}

void AChara::Reescale(float scaleVector)
{
	SetActorScale3D(FVector::OneVector * scaleVector);
	maxSpeed *= scaleVector;
}

// Called to bind functionality to input
void AChara::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

