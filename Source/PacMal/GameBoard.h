// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"
#include <Runtime/Core/Public/Math/Vector2D.h>

#include "Board.h"

#include "GameBoard.generated.h"

UCLASS()
class PACMAL_API AGameBoard : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY(VisibleAnywhere)
	FBoard board;

public:
	UPROPERTY(EditAnywhere, Category = "Map Load")
	FString filename;
	UPROPERTY(EditAnywhere, Category = "Map Load")
	TSubclassOf<AActor> solidTileClass;
	UPROPERTY(EditAnywhere, Category = "Map Load")
	TSubclassOf<AActor> dotTileClass;
	UPROPERTY(EditAnywhere, Category = "Map Load")
	TSubclassOf<AActor> powerTileClass;

	UPROPERTY(EditAnywhere, Category = "Characters")
	class APacPawn * pacmanPawn;
	UPROPERTY(EditAnywhere, Category = "Characters")
	class ABlinkyPawn * blinkyPawn;
	UPROPERTY(EditAnywhere, Category = "Characters")
	class AInkyPawn * inkyPawn;
	UPROPERTY(EditAnywhere, Category = "Characters")
	class APinkyPawn * pinkyPawn;
	UPROPERTY(EditAnywhere, Category = "Characters")
	class AClydePawn * clydePawn;

	UPROPERTY(EditAnywhere, Category = "Board Properties")
	float tileSize;

	UPROPERTY(EditAnywhere, Category = "Board Properties")
	TArray<float> ghostCycle;

	UPROPERTY()
	uint8 ghostCycleIndex = 0;

	UPROPERTY()
	float timeSinceStateChange = 0;

	UPROPERTY()
	TMap<FIntPoint, AActor*> visualTiles;

public:
	// Sets default values for this actor's properties
	AGameBoard();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = "Game Board")
	float GetWidth() { return board.GetColumns() * tileSize; }

	UFUNCTION(BlueprintPure, Category = "Game Board")
	float GetHeight() {	return board.GetRows() * tileSize; }

	UFUNCTION(BlueprintCallable, Category = "Game Board")
	void Initialize();

	UFUNCTION(BlueprintPure, Category = "Game Board")
	FVector GetCameraLocation(float fov);

	float GetTileSize() const { return tileSize; }

	FTile & GetTileFromLocation(const FVector & location);

	FTile & GetTileAt(const FIntPoint position);
	FTile & GetTileUp(const FIntPoint position);
	FTile & GetTileLeft(const FIntPoint position);
	FTile & GetTileDown(const FIntPoint position);
	FTile & GetTileRight(const FIntPoint position);

	FVector GetTileLocation(FIntPoint position);
	FIntPoint GetPositionFromLocation(FVector location);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void GenerateGeometry();
	void InitializePawns();
	void RescalePawns();
	void RelocatePawns();
	void UpdateGhostState(float Deltatime);
	void UpdateDotTiles();
	void CheckPacmanDeath();

};
