// Fill out your copyright notice in the Description page of Project Settings.

#include "PacPawn.h"
#include <Components/InputComponent.h>
#include "GameBoard.h"
#include "Board.h"

void APacPawn::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	InputComponent->BindAction("Up", IE_Pressed, this, &AChara::GoUp);
	InputComponent->BindAction("Down", IE_Pressed, this, &AChara::GoDown);
	InputComponent->BindAction("Left", IE_Pressed, this, &AChara::GoLeft);
	InputComponent->BindAction("Right", IE_Pressed, this, &AChara::GoRight);
}

void APacPawn::UpdateLocation(float DeltaTime)
{
	Super::UpdateLocation(DeltaTime);
	CheckDirection();
}

void APacPawn::CheckDirection()
{
	/*	The current direction is valid if the current tile has an compatible exit.
		If the current direction is NOT valid, stop the pawn if it's location is past the tile center.
	*/

	FTile & currentTile = gameBoard->GetTileFromLocation(GetActorLocation());

	// UE_LOG(LogTemp, Warning, TEXT("%s checks direction! %s"), *GetName(), *currentTile.ToString());

	switch (direction) {
	case MoveDirection::Up:
		if (!currentTile.Up() && gameBoard->GetTileLocation(currentTile.GetPosition()).X < GetActorLocation().X)
		{
			// NOT VALID
			SetActorLocation(gameBoard->GetTileLocation(currentTile.GetPosition()));
			Stop();
		}
		break;
	case MoveDirection::Down:
		if (!currentTile.Down() && gameBoard->GetTileLocation(currentTile.GetPosition()).X > GetActorLocation().X)
		{
			// NOT VALID
			SetActorLocation(gameBoard->GetTileLocation(currentTile.GetPosition()));
			Stop();
		}
		break;
	case MoveDirection::Left:
		if (!currentTile.Left() && gameBoard->GetTileLocation(currentTile.GetPosition()).Y > GetActorLocation().Y)
		{
			// NOT VALID
			SetActorLocation(gameBoard->GetTileLocation(currentTile.GetPosition()));
			Stop();
		}
		break;
	case MoveDirection::Right:
		if (!currentTile.Right() && gameBoard->GetTileLocation(currentTile.GetPosition()).Y < GetActorLocation().Y)
		{
			// NOT VALID
			SetActorLocation(gameBoard->GetTileLocation(currentTile.GetPosition()));
			Stop();
		}
		break;
	}
}

void APacPawn::GoUp()
{
	// UE_LOG(LogTemp, Warning, TEXT("%s tries to move up!"), *GetName());

	FTile & currentTile = gameBoard->GetTileFromLocation(GetActorLocation());
	if (currentTile.Up() && direction != MoveDirection::Up)
	{
		// Correct position if turning
		if (direction != MoveDirection::Down)
		{
			float correction = FMath::Abs(GetActorLocation().Y - gameBoard->GetTileLocation(currentTile.GetPosition()).Y);
			SetActorLocation(gameBoard->GetTileLocation(currentTile.GetPosition()) + FVector{ correction, 0.f, 0.f });
		}

		// Change direction
		direction = MoveDirection::Up;
		directionVector.Set(1.f, 0.f, 0.f);

	}
}

void APacPawn::GoDown()
{
	// UE_LOG(LogTemp, Warning, TEXT("%s tries to move down!"), *GetName());

	FTile & currentTile = gameBoard->GetTileFromLocation(GetActorLocation());
	if (currentTile.Down() && direction != MoveDirection::Down)
	{
		// Correct position if turning
		if (direction != MoveDirection::Up)
		{
			float correction = FMath::Abs(GetActorLocation().Y - gameBoard->GetTileLocation(currentTile.GetPosition()).Y);
			SetActorLocation(gameBoard->GetTileLocation(currentTile.GetPosition()) + FVector{ -correction, 0.f, 0.f });
		}

		// Change direction
		direction = MoveDirection::Down;
		directionVector.Set(-1.f, 0.f, 0.f);

	}
}

void APacPawn::GoLeft()
{
	// UE_LOG(LogTemp, Warning, TEXT("%s tries to move left!"), *GetName());

	FTile & currentTile = gameBoard->GetTileFromLocation(GetActorLocation());
	if (currentTile.Left() && direction != MoveDirection::Left)
	{
		// Correct position
		if (direction != MoveDirection::Right)
		{
			float correction = FMath::Abs(GetActorLocation().X - gameBoard->GetTileLocation(currentTile.GetPosition()).X);
			SetActorLocation(gameBoard->GetTileLocation(currentTile.GetPosition()) + FVector{ 0.f, -correction, 0.f });
		}

		// Change direction
		direction = MoveDirection::Left;
		directionVector.Set(0.f, -1.f, 0.f);

		// UE_LOG(LogTemp, Warning, TEXT("%s changed direction!"), *GetName());

	}
}

void APacPawn::GoRight()
{
	// UE_LOG(LogTemp, Warning, TEXT("%s tries to move right!"), *GetName());

	FTile & currentTile = gameBoard->GetTileFromLocation(GetActorLocation());
	if (currentTile.Right() && direction != MoveDirection::Right)
	{
		// Correct position
		if (direction != MoveDirection::Left)
		{
			float correction = FMath::Abs(GetActorLocation().X - gameBoard->GetTileLocation(currentTile.GetPosition()).X);
			SetActorLocation(gameBoard->GetTileLocation(currentTile.GetPosition()) + FVector{ 0.f, correction, 0.f });
		}

		// Change direction
		direction = MoveDirection::Right;
		directionVector.Set(0.f, 1.f, 0.f);

	}
}
