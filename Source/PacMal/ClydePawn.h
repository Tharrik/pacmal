// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GhostPawn.h"
#include "ClydePawn.generated.h"

/**
 * 
 */
UCLASS()
class PACMAL_API AClydePawn : public AGhostPawn
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Ghost")
		float fearFactor = 8;

public:
	virtual void FindTarget() override;

};
